
class AuthStoreClass {

  isAuthenticated() {
    if (localStorage.getItem('token')) {
      return true;
    }
    return false;

  }

  setUser(username) {
      localStorage.setItem('username', JSON.stringify(username));
  }

  setToken(token) {
      localStorage.setItem('token', token);
  }

  getUser() {
    return JSON.parse(localStorage.getItem("username"));
  }

  getToken() {
    return localStorage.getItem('token');
  }

  removeUser() {
    localStorage.removeItem('username');
    localStorage.removeItem('token');
  }

}

export default new AuthStoreClass();