import AuthStoreClass from './AuthStore'

/*eslint-disable no-console */
const APP_ID = '68a4sd6a-8w7r-9s6w-q1w2-g3h5h9t8y4yu';

class API {

    headers = {
        'Content-Type'           : 'application/json',
        'Accept'                 : '*/*',
        'x-parse-application-id' : APP_ID
    };



    getHeader(){
        if(AuthStoreClass.isAuthenticated()) this.headers['x-parse-session-token'] = AuthStoreClass.getToken();
        return this.headers;
    }

    getApiID(){
        return APP_ID;
    }

    deleteAuthorization(){
        delete this.headers['x-parse-session-token'];
    }

    get(url, query){
        let params = {
            method: 'get',
            headers: this.getHeader()
        };
        if(query){
            let esc = encodeURIComponent;
            let _query = Object.keys(query)
                .map(k => esc(k) + '=' + esc(query[k]))
                .join('&');
            url += '?' + _query
        }
        return this.invoke(url, params);
    }

    put(url, body){
        let params = {
            method  : 'put',
            headers : this.getHeader(),
            body    : JSON.stringify(body)
        };
        return this.invoke(url, params);
    }

    post(url, body){

        let params = {
            method   : 'post',
            headers  : this.getHeader(),
            body     : JSON.stringify(body)
        };
        return this.invoke(url, params);

    }

    delete(url){
        let params = {
            method  : 'delete',
            headers : this.getHeader()
        };
        return this.invoke(url, params);
    }

    invoke(url, params){
        return new Promise((resolve, reject) => {
            fetch(url, params)
                .then(response => {

                    if(response.ok) {

                        response.json().then(responseData => {

                            resolve(responseData)
                        })

                    }else{

                        response.json().then(error => {

                            reject(error)

                        })

                    }

                }).catch(error => {
                    console.log(error);
                    reject(error)
                })
        })
    }
}
export default new API();