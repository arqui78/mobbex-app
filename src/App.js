import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.css'
import io          from 'socket.io-client';
import moment      from 'moment-timezone';
import uuid        from 'uuid/v4';
import FacebookLogin from 'react-facebook-login';
import GoogleLogin from 'react-google-login';
// import queryString from 'query-string'
// import renderHTML from 'react-render-html';

import './App.css';

import ApiService from './ApiService'
import AuthStore  from './AuthStore';

const BASE_URL = 'http://localhost:1350'

let socket;
class App extends Component {

  constructor(props) {

    super(props)

    const vm = this

    vm.state = {
      messageSuccess : '',
      messageError   : '',
      fields : {
        email      : '',
        password   : '',
        experience : '',
        schedule   : '',
        date       : '',
        adventurous: ''
      },
      experiences        : [],
      experienceSelected : null,
      mobbex:{
        active:false,
        body:''
      }
    }

    this.BASE_URL = window.location.hostname === 'localhost' || window.location.hostname === '127.0.0.1' ? BASE_URL : window.location.origin;

    this.onChange       = this.onChange.bind(this)
    this.managerMessage = this.managerMessage.bind(this)
    this.login          = this.login.bind(this)
    this.login_facebook = this.login_facebook.bind(this)
    this.login_google   = this.login_google.bind(this)
    this.logout         = this.logout.bind(this)

    this.getExperiences = this.getExperiences.bind(this)
    this.postCheckout   = this.postCheckout.bind(this)
    this.deletePayment  = this.deletePayment.bind(this)
    this.createBooking  = this.createBooking.bind(this)

    this.initMobbexPayment   = this.initMobbexPayment.bind(this)
    this.renderMobbexButton  = this.renderMobbexButton.bind(this)
    this.renderWindowsMobBex = this.renderWindowsMobBex.bind(this)




  }

  componentWillMount(){

    if(AuthStore.isAuthenticated()){
      this.getExperiences()
      this.connectSocket()
    }

  }

  componentWillUnmount() {}

   componentWillReceiveProps (nextProps) {

    if(nextProps.isGeolocationAvailable && nextProps.isGeolocationAvailable && !this.state.setGeolocation) {
        this.setState({
            setGeolocation: nextProps.coords ? true : false,
            fields: {
                ...this.state.fields,
                latitude  : nextProps.coords ? nextProps.coords.latitude  : '',
                longitude : nextProps.coords ? nextProps.coords.longitude : ''
            }
        })
    }

    }

  onChange(event, key){

    event.preventDefault()

    this.setState({
      messageSuccess : '',
      messageError : '',
      fields:{
        ...this.state.fields,
        [key]: event.target.value
      }
    }, ()=>{

      if(key === 'experience'){

        this.setState({
          experienceSelected: this.state.experiences.find(el => {return el._id === this.state.fields.experience}),
          fields:{
            ...this.state.fields,
            schedule: null
          }
        })

      }
    })

  }

  login(event){

    event.preventDefault()

    let dataSend = {
      email    : this.state.fields.email,
      password : this.state.fields.password
    }

    ApiService.post(`${this.BASE_URL}/v1/authentication/login`, dataSend)
        .then( response => {

          AuthStore.setUser(response)
          AuthStore.setToken(response.sessionToken)

            this.setState({
              fields: {
                ...this.state.fields,
                email: '',
                password: '',
              }
            }, () => {
              this.getExperiences()
              this.connectSocket()
            })
        }).catch( this.managerMessage)
  }

  logout(){
    AuthStore.removeUser()
    this.setState({
      experiences  : [],
      payment: null,
      experienceSelected: null,
      fields:{
        ...this.fields,
        experience  : null,
        schedule    : null,
        adventurous : '',
        date        : ''
      },
      mobbex:{
        active: false,
        body:''
      }
    }, () =>{
      if(this.socket) {
        console.log("Disconnecting Socket")
        this.socket.disconnect()
      }
    })
  }

  getExperiences(){

    ApiService.get(`${this.BASE_URL}/v1/users/experiences`, {limit: 50})
        .then( response => {

          this.setState({
            experiences  : response.results,
          })

        }).catch( this.managerMessage)
  }

  postCheckout(event){

    event.preventDefault()

      console.log(this.state.fields)
    let dataToSend = {
      total       : this.state.experienceSelected.price,
      currency    : this.state.experienceSelected.currency.acronym,
      description : `Reserva de experiencia ${this.state.experienceSelected.name}`,
      return_url  : this.BASE_URL
    }
    ApiService.post(`${this.BASE_URL}/v1/payments`, dataToSend)
        .then( response => {

          this.setState({
            payment: response,
          }, this.initMobbexPayment)

        }).catch(this.managerMessage)
  }

  deletePayment(){

    ApiService.delete(`${this.BASE_URL}/v1/payments/${this.state.payment._id}`)
        .then( response => {

          this.setState({
            payment: null,
          })

        }).catch(this.managerMessage)
  }

  createBooking(event){

    if(event) event.preventDefault()

    let schedule = this.state.experienceSelected.schedules.find(el => {return el._id === this.state.fields.schedule})

    let _date  = moment(this.state.fields.date),
        since  = moment(schedule.since, 'HH:mm'),
        until  = moment(schedule.until, 'HH:mm'),
        date     = since.date( _date.date() ).toDate(),
        dateEnd  = until.date( _date.date() ).toDate();

    let dataToSend = {
      experienceId : this.state.experienceSelected._id,
      scheduleId   : this.state.fields.schedule,
      adventurous  : this.state.fields.adventurous,
      date         : date,
      dateEnd      : dateEnd,
      // return_url   : this.BASE_URL

    }

    ApiService.post(`${this.BASE_URL}/v1/bookings`, dataToSend)
        .then( response => {

          console.log(response)

          this.setState({
            payment: null,
            experienceSelected: null,
            fields:{
              ...this.fields,
              experience  : '',
              schedule    : '',
              adventurous : '',
              date        : ''
            },
            mobbex:{
              active: true,
              body:response
            }
          },this.renderWindowsMobBex)

        }).catch(this.managerMessage)
  }

  renderWindowsMobBex() {

    if (this.state.mobbex.body){

      let windowLogin = window.open(this.state.mobbex.body.url, '_blank');

      let timer = setInterval(function () {

        if (windowLogin.closed) {

          clearInterval(timer);
          console.log('close')

        }
      },1000)
    }

  }

  connectSocket(){
    let socketOpts = {
      autoConnect: true,
      query : {
        appId        : ApiService.getApiID(),
        sessionToken : AuthStore.getToken()
      }
    }
    socket = io.connect(this.BASE_URL, socketOpts);

    socket.on('connect', function(){

      console.log('socket connect')

    }).on('error', (error) =>{
      console.log('socket error')
      console.log(error)

    })
  }

  managerMessage(error, success){

    if(error){

      error = error.error ? error.error : error

      this.setState({
        messageError : error.message
            ? error.message
            : error.description
                ? error.description
                : 'Algo salio mal!. Intente mas tarde'
      }, () => {

        setTimeout( ()=>{
          this.setState({
            messageSuccess : '',
            messageError   : ''
          })
        }, 3000)
      })

    }else{

      this.setState({
        messageSuccess : success.message ? success.message : '',
      }, () => {

        setTimeout( ()=>{
          this.setState({
            messageSuccess : '',
            messageError   : ''
          })
        }, 3000)
      })

    }

  }

  onSelectSchedule = (schedule) =>{

    this.setState({
      fields:{
        ...this.fields,
        schedule: schedule._id
      }
    })
  }

  renderMobbexButton(){

    window.MobbexButton.render({
      checkout: this.state.payment.checkout,
      text: 'Pagar con Mobbex',
      image: 'https://res.mobbex.com/images/sources/mobbex.png',
      backgroundColor: 'rgb(137,0,255)',
      textColor: 'rgb(255, 255, 255)',
      textSize: '1rem',
      onPayment: (data) => {
        console.info('Payment: ', data);
        if(data.status === 200) this.createBooking()
        else this.managerMessage({})
      },
      onOpen: () => {
        console.info('Pago iniciado.');
      },
      onClose: (cancelled) => {
        console.info(`${cancelled ? 'Cancelado' : 'Cerrado'}`);
        this.deletePayment()
      },
      onError: this.managerMessage
    }, '#mbbx-button');

  }

  initMobbexPayment(){

    let mbbxButton = window.MobbexButton.init({
      checkout: this.state.payment.checkout,
      onPayment: (data) => {
        console.info('Payment: ', data);
          if(data.status === 200) this.createBooking()
          else this.managerMessage(data)
      },
      onOpen: () => {
        console.info('Pago iniciado.');
      },
      onClose: (cancelled) => {
        console.info(`${cancelled ? 'Cancelado' : 'Cerrado'}`);
        this.deletePayment()
      },
      onError: this.managerMessage
    });

    mbbxButton.open();

  }

  login_facebook (event) {

    const vm = this;

    event.preventDefault()

    let state = uuid()
    let url   = `${vm.BASE_URL}/v1/authentication/facebook?state=${state}`;
    let xPos  = ( window.innerWidth / 2 ) - 400;
    let yPos  = ( window.innerHeight / 2 ) - 400;

    var windowLogin = window.open(url, '', 'width=640, height=480, left=' + xPos + ', top=' + yPos + 'location=yes,status=yes');

    var timer = setInterval(function () {

      if (windowLogin.closed) {

        clearInterval(timer);

        ApiService.get(`${vm.BASE_URL}/v1/authentication/facebook/${state}`)
            .then( resp => {

              console.log(`response login facebook`)
              console.log(resp)

              if(resp.response_type === 'token'){

                AuthStore.setUser(resp)
                AuthStore.setToken(resp.sessionToken)

                vm.getExperiences()
                vm.connectSocket()

              }else{

                vm.managerMessage({
                  message: 'El usuario debe esta registrado para loguiar con una red social'
                })
              }

            }).catch( vm.managerMessage)

      }

    }, 1000);

  }

  login_google (event) {

    const vm = this;

    event.preventDefault()

    let state = uuid()
    let url   = `${vm.BASE_URL}/v1/authentication/google?state=${state}`;
    let xPos  = ( window.innerWidth / 2 ) - 400;
    let yPos  = ( window.innerHeight / 2 ) - 400;

    var windowLogin = window.open(url, '', 'width=640, height=480, left=' + xPos + ', top=' + yPos + 'location=yes,status=yes');

    var timer = setInterval(function () {

      if (windowLogin.closed) {

        clearInterval(timer);

        ApiService.get(`${vm.BASE_URL}/v1/authentication/google/${state}`)
            .then( resp => {

              console.log(`response login google`)
              console.log(resp)

              if(resp.response_type === 'token'){

                AuthStore.setUser(resp)
                AuthStore.setToken(resp.sessionToken)

                vm.getExperiences()
                vm.connectSocket()

              }else{

                vm.managerMessage({
                  message: 'El usuario debe esta registrado para loguiar con una red social'
                })
              }

            }).catch( vm.managerMessage)

      }

    }, 1000);

  }

  async networkProfile (network, accessToken) {

    const vm = this;

    const state = uuid();

    ApiService.get(`${vm.BASE_URL}/v1/authentication/${network}/profile?accessToken=${accessToken}&state=${state}&network=${network}`)
        .then( resp => {

          console.log(`response login ${network}`)
          console.log(resp)

          if(resp.response_type === 'token'){

            AuthStore.setUser(resp)
            AuthStore.setToken(resp.sessionToken)

            vm.getExperiences()
            vm.connectSocket()

          }else{

            vm.managerMessage({
              message: 'El usuario debe esta registrado para loguiar con una red social'
            })
          }

        }).catch( vm.managerMessage)

  }

  responseFacebook = response => {
    const vm = this;
    console.log('responseFacebook');
    console.log(response);
    if( response.accessToken ) vm.networkProfile('facebook', response.accessToken)

  };

  responseGoogle = (response) => {
    const vm = this;
    console.log('responseGoogle');
    console.log(response);
    if( response.accessToken ) vm.networkProfile('google', response.accessToken)
  };

  render() {

    return (
        <div className="App">
          <div className="container-messages">
            {
              this.state.messageSuccess &&
              <div className="alert alert-success" role="alert">
                {this.state.messageSuccess}
              </div>
            }
            {
              this.state.messageError &&
              <div className="alert alert-danger" role="alert">
                {this.state.messageError}
              </div>
            }
          </div>
          <div className="container-fluid app-container">
            {
              AuthStore.isAuthenticated() &&
              <div className="button-close" onClick={ (event) => { this.logout() } }>
                <span>X</span>
              </div>
            }

            {
              AuthStore.isAuthenticated() ? (
                  <div className="App-experiences">

                    <strong>Reservar Experiencia</strong>

                    <div className="App-experiences-container">

                      <div className="experiences-container">

                        <form autoComplete="off">
                          <div className="form-group text-left">
                            <label htmlFor="selectExperience">Experiencias</label>
                            <select
                                className="form-control"
                                id="selectExperience"
                                onChange={ event => this.onChange(event, 'experience')}
                                value={this.state.fields.experience}

                            >
                              <option value="">Seleccione una opción</option>
                              {
                                this.state.experiences.map((option, key) =>

                                    <option value={option._id} key={key}>{option.name}</option>
                                )
                              }
                            </select>
                          </div>

                          {

                            this.state.experienceSelected &&
                                <div className="">

                                  <div className="form-group text-left">
                                    <label htmlFor="priceExperience">Precio</label>
                                    <div id="priceExperience" style={{padding: 5, border: '1px solid', borderRadius: 6}}>
                                      <span>{`${this.state.experienceSelected.price} ${this.state.experienceSelected.currency.acronym}`}</span>
                                    </div>
                                  </div>


                                  <div className="form-group text-left">
                                    <label htmlFor="selectSchedule">Turnos</label>
                                    <div className="selectSchedules" id="selectSchedule">
                                      {
                                        this.state.experienceSelected.schedules.map((option, key) =>

                                            <div className={`schedule ${option._id === this.state.fields.schedule ? 'schedule-select'  : ''}`} key={key} onClick={()=>{this.onSelectSchedule(option)}}>{`Turno ${key + 1 } - ${option.since}`}</div>
                                        )
                                      }
                                    </div>

                                  </div>

                                  {
                                    this.state.fields.schedule &&
                                     <div>

                                       <div className="form-group text-left">
                                         <label htmlFor="inputAdventurous">Aventureros</label>
                                         <input id="inputAdventurous" type="number" className="form-control" placeholder="Aventureros" onChange={ event => this.onChange(event, 'adventurous')} autoComplete="off"/>
                                       </div>

                                       <div className="form-group text-left">
                                         <label htmlFor="inputDate">Fecha</label>
                                         <input
                                             id="inputDate"
                                             type="date"
                                             className="form-control"
                                             placeholder="Aventureros"
                                             onChange={ event => this.onChange(event, 'date')}
                                             autoComplete="off"
                                             min={moment().add(1, 'day').format('YYYY-MM-DD')}
                                         />
                                       </div>

                                       <button id="checkout-button" onClick={this.createBooking} disabled={!this.state.fields.adventurous || !this.state.fields.date}>Pagar con Mobbex</button>
                                       <div id="mbbx-button"></div>
                                       <div id="mbbx-container"></div>
                                     </div>
                                  }

                                </div>

                          }

                        </form>

                      </div>

                      {/*{*/}
                      {/*  this.state.mobbex.active && <div>{renderHTML(this.state.mobbex.body)}</div>*/}
                      {/*}*/}

                    </div>

                  </div>
              ) : null
            }

            {
              !AuthStore.isAuthenticated() ? (
                  <div className="App-login">
                    <strong >Autentificación</strong>
                    <form autoComplete="off" onSubmit={this.login}>
                      <div className="form-group text-left">
                        <label htmlFor="inputEmail">Email</label>
                        <input id="inputEmail" type="email" className="form-control" placeholder="Email" onChange={ event => this.onChange(event, 'email')} autoComplete="off"/>
                      </div>
                      <div className="form-group text-left">
                        <label htmlFor="inputPassword">Contraseña</label>
                        <input id="inputPassword" type="password" className="form-control" placeholder="*******" onChange={ event => this.onChange(event, 'password')} autoComplete="off"/>
                      </div>
                      <button className="btn btn-primary" disabled={!this.state.fields.email || !this.state.fields.password} onClick={this.login}>Enviar</button>
                    </form>
                    <br/>
                    {/*<button className="btn btn-primary" onClick={this.login_facebook}>Facebook</button>*/}
                    {/*<br/>*/}
                    {/*<br/>*/}
                    {/*<button className="btn btn-primary" onClick={this.login_google}>Google</button>*/}
                    <FacebookLogin
                        appId=""
                        autoLoad={false}
                        cssClass="btn btn-primary"
                        fields="name,email,picture"
                        callback={this.responseFacebook}
                        icon="fa-facebook pr-3"
                        textButton="Facebook"
                    />
                    <br/>
                    <br/>
                    <GoogleLogin
                        className="btn btn-primary"
                        clientId=""
                        buttonText="Google"
                        onSuccess={this.responseGoogle}
                        onFailure={this.responseGoogle}
                        icon

                    />
                  </div>
              ) : null
            }

          </div>

        </div>
    );
  }

}

export default App;
